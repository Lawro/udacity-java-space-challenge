// Rocket class
public class Rocket implements Spaceship {
    private int cost;
    private int weight;
    private int maxWeight;
    private double launchExp;
    private double landCrash;
    private double cargo_carried;
    private double cargo_limit;

    // Constructor
    Rocket() {
        this.setCost(0);
        this.setWeight(0);
        this.setMaxWeight(0);
        this.setLaunchExp(0.0);
        this.setLandCrash(0.0);
        this.setCargoCarried(0.0);
        this.setCargoLimit(0.0);
    }

    public void setCost(int value) {
        this.cost = value;
    }

    public int getCost() {
        return this.cost;
    }

    public void setWeight(int rocketWeight) {
        this.weight = rocketWeight;
    }

    public int getWeight() {
        return this.weight;
    }

    public void setMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }

    public int getMaxWeight() {
        return this.maxWeight;
    }

    public void setLaunchExp(double perc) {
        this.launchExp = perc;
    }

    public double getLaunchExp() {
        return this.launchExp;
    }

    public void setLandCrash(double perc) {
        this.landCrash = perc;
    }

    public double getLandCrash() {
        return this.landCrash;
    }

    public void setCargoCarried(double itemWeight) {
        this.cargo_carried += itemWeight;
    }

    public double getCargoCarried() {
        return this.cargo_carried;
    }

    public void setCargoLimit(double cargoTotal) {
        this.cargo_limit = cargoTotal;
    }

    public double getCargoLimit() {
        return this.cargo_limit;
    }

    public boolean launch() {
        return true;
    }

    public boolean land() {
        return true;
    }

    // Tests to see if rocket can carry more items
    public boolean canCarry(Item item) {
        double itemWeight = item.getWeight();
        if (itemWeight + cargo_carried > cargo_limit) {
            return false;
        }
        else {
            carry(item);
            return true;
        }
    }

    // Adds item to cargo
    public void carry(Item item) {
        cargo_carried += item.getWeight();
    }
}
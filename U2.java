import java.util.Random;

// U2 rocket
public class U2 extends Rocket {

    // Constructor
    U2 () {
        this.setCost(120);
        this.setWeight(18000);
        this.setMaxWeight(29000);
        this.setLaunchExp(0.04);
        this.setLandCrash(0.08);
        this.setCargoCarried(0);
        this.setCargoLimit(this.getMaxWeight() - this.getWeight());
    }

    // Returns launch explosion probability outcome
    public boolean launch() {
        if (new Random().nextDouble() <= 0.04 * (getCargoCarried() / getCargoLimit())) {
            return false;
        }
        else return true;
    }

    // Returns crash landing probability outcome
    public boolean land() {
        if (new Random().nextDouble() <= 0.08 * (getCargoCarried() / getCargoLimit())) {
            return false;
        }
        else return true;
    }
}
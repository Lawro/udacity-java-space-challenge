/* Items carry a weight that will fill up
   rockets
*/
public class Item {
    private String name = "";
    private int weight = 0;

    Item () {
        this.name = "";
        this.weight = 0;
    }

    public void setName(String newName) {
        name = newName;
    }

    public String getName() {
        return name;
    }

    public void setWeight(int itemWeight) {
        weight = itemWeight;
    }

    public int getWeight() {
        return weight;
    }
}
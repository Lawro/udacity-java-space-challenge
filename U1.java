import java.util.Random;

// U1 rocket
public class U1 extends Rocket {

    // Constructor
    U1 () {
        this.setCost(100);
        this.setWeight(10000);
        this.setMaxWeight(18000);
        this.setLaunchExp(0.05);
        this.setLandCrash(0.01);
        this.setCargoCarried(0);
        this.setCargoLimit(this.getMaxWeight() - this.getWeight());
    }

    // Returns launch explosion probability outcome
    public boolean launch() {
        if (new Random().nextDouble() <= 0.05 * (getCargoCarried() / getCargoLimit())) {
            return false;
        }
        else return true;
    }

    // Returns crash landing probability outcome
    public boolean land() {
        if (new Random().nextDouble() <= 0.01 * (getCargoCarried() / getCargoLimit())) {
            return false;
        }
        else return true;
    }
}